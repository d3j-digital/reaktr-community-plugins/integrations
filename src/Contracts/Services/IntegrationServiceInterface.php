<?php

namespace D3JDigital\Integrations\Contracts\Services;

interface IntegrationServiceInterface
{
    /**
     * @param string $ident
     * @param string $name
     * @param string $entrypointInterface
     * @param string $description
     * @return bool
     */
    public function registerIntegration(string $ident, string $name, string $entrypointInterface, string $description): bool;

    /**
     * @param string $ident
     * @param array $params
     * @return mixed
     */
    public function loadIntegration(string $ident, array $params = []): mixed;
}
