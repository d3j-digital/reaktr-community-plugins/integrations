<?php

namespace D3JDigital\Integrations\Services;

use D3JDigital\Integrations\Contracts\Services\IntegrationServiceInterface;

class IntegrationService implements IntegrationServiceInterface
{
    protected ?array $integrations = [];

    public function registerIntegration(string $ident, string $name, string $entrypointInterface, string $description): bool
    {
        if (interface_exists($entrypointInterface)) {
            $this->integrations[$ident] = [
                'ident' => $ident,
                'name' => $name,
                'description' => $description,
                'entrypoint' => $entrypointInterface,
            ];
            return true;
        }
        return false;
    }

    public function loadIntegration(string $ident, array $params = []): mixed
    {
        if (isset($this->integrations[$ident])) {
            $entrypoint = $this->integrations[$ident]['entrypoint'];

            /** @var $entrypoint */
            return app()->make($entrypoint, $params);
        }
        return null;
    }
}
