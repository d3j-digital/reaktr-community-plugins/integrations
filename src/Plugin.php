<?php

namespace D3JDigital\Integrations;

use OpenSourceDeveloper\Reaktr\Core\Abstracts\Plugin as ReaktrPlugin;
use D3JDigital\Integrations\Contracts\Services\IntegrationServiceInterface;
use D3JDigital\Integrations\Services\IntegrationService;

class Plugin extends ReaktrPlugin
{
    public ?string $pluginName = 'Integrations';
    public ?string $pluginKey = 'integrations';
    public ?string $pluginVendorName = 'D3JDigital';
    public ?string $pluginVendorKey = 'd3j-digital';
    public ?string $pluginVersion = 'v0.0.1';
    public ?string $pluginDescription = 'Adds integration functionality';
    public ?array $pluginDependencies = [];

    public array $singletons = [
        IntegrationServiceInterface::class => IntegrationService::class,
    ];
}
